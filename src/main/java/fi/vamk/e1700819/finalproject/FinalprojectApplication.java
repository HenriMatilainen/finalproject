package fi.vamk.e1700819.finalproject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


@SpringBootApplication
public class FinalprojectApplication {
	@Autowired
	private TicketRepository repository;
	public static void main(String[] args) {
		SpringApplication.run(FinalprojectApplication.class, args);
	}


	@Bean
	public void initData(){
		Calendar c = new GregorianCalendar(2020, 04, 30);
		Ticket f = new Ticket("Flight A", "Origin a", "Destination B", c);
		Ticket f3 = new Ticket("Flight B", "Origin B", "Destination C", c);
		repository.save(f);
		repository.save(f3);
		Iterable<Ticket> ff = repository.findAll();
		for(Ticket f2:ff){
			System.out.println(f2.toString());
		}
	}

}
