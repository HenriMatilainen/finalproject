package fi.vamk.e1700819.finalproject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "Tickets CRUD.", description = "Provides RESTful API for ticket.")
@RestController
public class TicketController {
    @Autowired
    private TicketRepository repository;
    @ApiOperation(value= "Returns all the tickets")
    @GetMapping("/bookings")
    public Iterable<Ticket> list(){
        return repository.findAll();
    }
    /*@GetMapping("/customerview")
    public Iterable<Ticket> customerList(){
        return repository.findByName(name);
    } */ 
}