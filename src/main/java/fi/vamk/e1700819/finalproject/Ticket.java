package fi.vamk.e1700819.finalproject;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.persistence.Entity;

@Entity
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String destination;
    private String origin;
    private Calendar date;

    public Ticket() {
    }

    public Ticket(String name, String destination, String origin, Calendar date) {
        this.name = name;
        this.destination = destination;
        this.origin = origin;
        this.date = date;
    }


    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getDestination() {
        return this.destination;
    }
    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getOrigin() {
        return this.origin;
    }
    public void setOrigin(String origin) {
        this.origin = origin;
    }
    public Calendar getDate(){
        return this.date;
    }
    public void setDate(Calendar date) {
        this.date = date;
    }

    @Override
    public String toString() {


        return "{" +
            " id='" + getId() + "'" +
            ", name='" + getName() + "'" + ", origin='" + getOrigin() + "'" + ", destination='" + getDestination() + "'" +
            "}";
    }
}