package fi.vamk.e1700819.finalproject;

import org.springframework.data.repository.CrudRepository;

public interface TicketRepository extends CrudRepository<Ticket, Integer> {

    public Iterable<Ticket> findByName(String name);
    public Iterable<Ticket> findByDestination(String destination);
    public Iterable<Ticket> findByOrigin(String origin);

}
